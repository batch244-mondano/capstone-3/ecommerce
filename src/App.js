// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashBoard from "./pages/AdminDashBoard";
import './App.css';
import { UserProvider } from './UserContext';


function App() {

    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

    // Used to check if the user information is properly stored upon login and localStorage information is cleared upon logout
    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

  return(
    // In React JS, multiple components rendered in a single component should be wrapped in a parent component
    // "Fragment" ensures that an error will be prevented

    // We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop
    // All information inside the value prop will be accessible to pages/components wrapped around the UserProvider.
    // It allows re-rendering when the "value" prop changes.
    <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <AppNavbar/>
            {/*<Home/>*/}
                <Container className="jusify-content-center">
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/products/:productId" element={<ProductView/>} />
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/register" element={<Register/>} />
                    <Route path="/logout" element={<Logout/>} />
                {/* "*" is used to render paths that are not found in our routing system/}*/}
                    <Route path="*" element={<Error />} />
                    <Route path="/admin" element={<AdminDashBoard />} />
                </Routes>
                </Container>
            </Router>
    </UserProvider>
  );
}

export default App;