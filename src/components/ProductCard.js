// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}) {

	// Decontruct the course properties into their own variables
	const { _id, name, description, price} = productProp;

    return (
    <Row>
			<Col>
			    <Card className="border border-dark col m-3">
			        <Card.Body >
			            <Card.Title>{name}</Card.Title>
			            <Card.Subtitle>Description:</Card.Subtitle>
			            <Card.Text>{description}</Card.Text>
			            <Card.Subtitle>Price:</Card.Subtitle>
			            <Card.Text>Php {price}</Card.Text>
			            <Button as={Link} variant="primary" to={`/products/${_id}`}>Order</Button>
			        </Card.Body>
			    </Card>  
			</Col>
	</Row>
    );
}