import {Col, Row} from 'react-bootstrap';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import '../App.css';



export default function Home() {

	const data = {
		title : "LechonArn Specials",
		content : "The best delicacies/pasalubong in Ormoc!",
		destination: "/products",
		label: "Order Now!"
	}
	return (
		<>
		<Row>
			<Col className="home container-fluid">
              	<Banner className="home-field" data={data}/>
              	<Highlights className="home-field"/>
            </Col>
        </Row>
		</>
	)
}